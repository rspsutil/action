package util

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Allows for a single structure to contain multiple strings that you can consider all "equal" to each other.
 *
 * @param [aliases] The strings that will be compared for "equality"
 * @param [ignoreCase] Whether or not you want to ignore case when checking for equality
 */
class StringAliases(private val aliases: Set<String>, private val ignoreCase: Boolean) {

    constructor(aliases: List<String>, ignoreCase: Boolean = false):
            this(aliases.toSet(), ignoreCase)

    constructor(vararg aliases: String, ignoreCase: Boolean):
            this(aliases.toSet(), ignoreCase)

    /**MODIFICATION TO ORIGINAL**/
    var beingChecked: Boolean = false

    override operator fun equals(other: Any?): Boolean =
        when (other) {
            is String -> other in this
            is StringAliases -> aliases.any { it in other }
            else -> false
        }

    operator fun contains(other: String) = aliases.any { it.equals(other, ignoreCase) }


    override fun hashCode(): Int = (31 * aliases.hashCode()) + ignoreCase.hashCode()

    override fun toString(): String = "StringAliases$aliases"

    /** MODIFICATION TO ORIGINAL **/
    infix fun intersects(other: StringAliases) = this == other
}
