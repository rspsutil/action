default_params = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
default_subject = "SUBJECT"
default_class_name = "Action"
N = "\n"
T = "\t"
NT = N + T
NTT = NT + T
CNT = "," + NT
argumentTypes = "argumentTypes: List<KClass<out Any>>"
subjectRequirements = "subjectRequirements: List<Requirement<{}>> = listOf()"
actionFunction = "override inline val actionFunction: {}.{} -> Unit"
onInvalidArgumentCount = "override inline val onInvalidArgumentCount: (Int) -> Unit = {}"
onInvalidArgumentTypes = "override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {}"


class InfiStr:
    def __init__(self, string: str):
        self.string = string

    def __getitem__(self, item):
        return self.string


def subject_arg(subject: str = default_subject) -> str:
    return "<" + subject + ">"


def create_type_args(
        *,
        param_amount: int,
        brackets: str = "<>",
        subject: str = default_subject,
        subject_prefix: str = "",
        subject_postfix: str = "",
        param_chars: str = default_params,
        param_prefix: str = "",
        param_postfix: str = "",
        use_subject: bool = True,
        use_brackets: bool = True
) -> str:
    if param_amount < 0:
        if use_subject:
            return subject_arg(subject)
        return ""

    param_string = ""
    if use_brackets:
        param_string += brackets[0]
    if use_subject:
        param_string += subject_prefix + subject + subject_postfix
        if param_amount > 0:
            param_string += ", "
    for param_index in range(param_amount):
        param_string += param_prefix.format(param_index) + param_chars[param_index] + param_postfix.format(param_index)
        if param_index == param_amount - 1:
            break
        param_string += ", "

    if use_brackets:
        param_string += brackets[1]
    return param_string


def create_class(
        *,
        param_amount: int,
        class_name: str = default_class_name,
        subject: str = default_subject,
        param_chars: str = default_params
) -> str:
    any_params = InfiStr("Any?")

    type_args = create_type_args(
        param_amount=param_amount,
        subject=subject,
        param_chars=param_chars,
        param_postfix=": Any"
    )
    class_str = "class "
    class_str += class_name
    class_str += str(param_amount)
    class_str += type_args
    class_str += "(" + NT

    if param_amount > 0:
        class_str += argumentTypes + CNT

    class_str += subjectRequirements.format(subject) + CNT
    class_str += onInvalidArgumentCount + CNT
    class_str += onInvalidArgumentTypes + CNT
    class_str += actionFunction.format(
        subject,
        create_type_args(
            param_amount=param_amount,
            param_chars=param_chars,
            use_subject=False,
            brackets="()"
        )
    )
    class_str += N
    class_str += "): Action"
    class_str += subject_arg(subject=subject)
    class_str += "(" + NT
    class_str += "argumentCount = " + str(param_amount) + CNT
    class_str += "argumentTypes = " + ("argumentTypes" if param_amount > 0 else "listOf()") + CNT
    class_str += "subjectRequirements = subjectRequirements" + CNT
    class_str += "onInvalidArgumentCount = onInvalidArgumentCount" + CNT
    class_str += "onInvalidArgumentTypes = onInvalidArgumentTypes" + CNT
    class_str += "actionFunction = actionFunction"
    class_str += N
    class_str += ") {" + NT
    class_str += "fun execute"
    class_str += create_type_args(
        param_amount=param_amount,
        subject_prefix="subject: ",
        brackets="()",
        param_prefix="arg{}: ",
        param_chars=any_params
    )
    class_str += " =\n\t\t"
    class_str += "actionFunction" + create_type_args(
        param_amount=param_amount,
        subject="subject",
        brackets="()",
        param_prefix="arg{} as "
    )
    class_str += N
    class_str += "}"
    return class_str


def create_builders(
        *,
        param_amount: int,
        builder_name: str = default_class_name,
        subject: str = default_subject,
        param_chars: str = default_params
) -> str:
    builder_string = "inline fun "
    builder_string += create_type_args(
        param_amount=param_amount,
        param_chars=param_chars,
        param_prefix="reified ",
        param_postfix=": Any",
        subject=subject,
    )

    builder_string += " " + builder_name + "(" + NT
    builder_string += subjectRequirements.format(subject) + CNT
    builder_string += onInvalidArgumentCount.replace("override inline val ", "noinline ") + CNT
    builder_string += onInvalidArgumentTypes.replace("override inline val ", "noinline ") + CNT
    builder_string += "noinline actionFunction: " + subject + "."
    builder_string += create_type_args(
        param_amount=param_amount,
        use_subject=False,
        brackets="()"
    ) + " -> Unit" + N
    builder_string += ") = Action" + str(param_amount) + "(" + NT

    if param_amount > 0:
        builder_string += "argumentTypes = listOf"
        builder_string += create_type_args(
            param_amount=param_amount,
            param_postfix="::class",
            use_subject=False,
            brackets="()"
        ) + CNT
    builder_string += "subjectRequirements = subjectRequirements" + CNT
    builder_string += "onInvalidArgumentCount = onInvalidArgumentCount" + CNT
    builder_string += "onInvalidArgumentTypes = onInvalidArgumentTypes" + CNT
    builder_string += "actionFunction = actionFunction" + N
    builder_string += ")"
    return builder_string


def actual_builders():
    with open("ActionHelpers.kt", "w") as file:
        file.write("package action"+N+N)
        file.write("import action.requirement.Requirement"+N+N)
        for builder_index in range(21):
            file.write(create_builders(param_amount=builder_index) + N + N)


def actual_classes():
    with open("Action.kt", "w") as file:
        file.write("""@file:Suppress("UNCHECKED_CAST")

package action

import action.requirement.Requirement
import kotlin.reflect.KClass

""")
        for param_amount in range(21):
            file.write(create_class(param_amount=param_amount) + N + N)


actual_builders()