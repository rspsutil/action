package action.map

import action.Action
import action.Action1
import action.requirement.Requirement
import util.StringAliases
import kotlin.random.Random

class StringAliasIntersection(message: String): Exception(message)

class ActionMap<SUBJECT>(
    aliases: List<StringAliases>,
    actions: List<Action<SUBJECT>>,
    private inline val actionNotFound: Action1<SUBJECT, String>
) {

    private val actionMap: Map<StringAliases, Action<SUBJECT>>

    init {
        val map: HashMap<StringAliases, Action<SUBJECT>> = HashMap()
        (aliases zip actions).forEach { map.putIfAbsent(it.first, it.second) }
        map.
            asSequence().
            firstOrNull { (alias, action) ->
                alias.beingChecked = true
                val result = map.any {
                    !it.key.beingChecked &&
                    alias intersects it.key &&
                    action.actionSignature == it.value.actionSignature
                }
                alias.beingChecked = false
                result
            }.
            run {
                if (this != null)
                    throw StringAliasIntersection("String Alias intersection and signature match for separate actions in the same ActionMap.\n\tOffending Aliases: $key\n\tOffending Signature: ${value.actionSignature}")
            }
        this.actionMap = map
    }

    private fun getActionForNameAndArgs(actionName: String, actionArgs: List<Any?>): Action<SUBJECT> {
        val nameMatchedActions = actionMap.asSequence().
            filter { actionName in it.key }.
            map { it.value }.
            toList()

        for (action in nameMatchedActions) {
            if (action.allArgTypesMatch(actionArgs)) {
                return action
            }
        }

        return actionNotFound
    }

    operator fun invoke(target: SUBJECT, actionName: String, args: List<Any?>) {
        val actionToExecute = getActionForNameAndArgs(actionName, args)
        if (actionToExecute == actionNotFound)
            return actionNotFound(target, actionName)
        actionToExecute(target, args)
    }

    operator fun invoke(target: SUBJECT, actionName: String, vararg args: Any?) =
        this(target, actionName, args.asList())
}
