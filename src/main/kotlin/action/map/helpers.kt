package action.map

import action.Action
import action.Action1
import util.StringAliases

fun <SUBJECT> actionMapOf(
    vararg entries: Pair<StringAliases, Action<SUBJECT>>,
    actionNotFound: Action1<SUBJECT, String>
): ActionMap<SUBJECT> =
    ActionMap(
        aliases = entries.map { it.first },
        actions = entries.map { it.second },
        actionNotFound = actionNotFound
    )
