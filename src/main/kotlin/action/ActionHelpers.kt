package action

import action.requirement.Requirement

fun <SUBJECT> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	onInvalidArgumentCount: (Int) -> Unit = {},
	onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	actionFunction: SUBJECT.() -> Unit
) = Action0(
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A) -> Unit
) = Action1(
	argumentTypes = listOf(A::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B) -> Unit
) = Action2(
	argumentTypes = listOf(A::class, B::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C) -> Unit
) = Action3(
	argumentTypes = listOf(A::class, B::class, C::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D) -> Unit
) = Action4(
	argumentTypes = listOf(A::class, B::class, C::class, D::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E) -> Unit
) = Action5(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F) -> Unit
) = Action6(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G) -> Unit
) = Action7(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H) -> Unit
) = Action8(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I) -> Unit
) = Action9(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J) -> Unit
) = Action10(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K) -> Unit
) = Action11(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L) -> Unit
) = Action12(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M) -> Unit
) = Action13(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N) -> Unit
) = Action14(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any, reified O: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O) -> Unit
) = Action15(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class, O::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any, reified O: Any, reified P: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P) -> Unit
) = Action16(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class, O::class, P::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any, reified O: Any, reified P: Any, reified Q: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q) -> Unit
) = Action17(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class, O::class, P::class, Q::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any, reified O: Any, reified P: Any, reified Q: Any, reified R: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R) -> Unit
) = Action18(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class, O::class, P::class, Q::class, R::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any, reified O: Any, reified P: Any, reified Q: Any, reified R: Any, reified S: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S) -> Unit
) = Action19(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class, O::class, P::class, Q::class, R::class, S::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

inline fun <SUBJECT, reified A: Any, reified B: Any, reified C: Any, reified D: Any, reified E: Any, reified F: Any, reified G: Any, reified H: Any, reified I: Any, reified J: Any, reified K: Any, reified L: Any, reified M: Any, reified N: Any, reified O: Any, reified P: Any, reified Q: Any, reified R: Any, reified S: Any, reified T: Any> Action(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	noinline onInvalidArgumentCount: (Int) -> Unit = {},
	noinline onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	noinline actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T) -> Unit
) = Action20(
	argumentTypes = listOf(A::class, B::class, C::class, D::class, E::class, F::class, G::class, H::class, I::class, J::class, K::class, L::class, M::class, N::class, O::class, P::class, Q::class, R::class, S::class, T::class),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
)

