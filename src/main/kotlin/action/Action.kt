@file:Suppress("UNCHECKED_CAST")

package action

import action.requirement.Requirement
import kotlin.reflect.KClass

sealed class Action<SUBJECT>(
	val argumentCount: Int,
	val argumentTypes: List<KClass<out Any>>,
	val subjectRequirements: List<Requirement<SUBJECT>>,
	open inline val onInvalidArgumentCount: (Int) -> Unit = {},
	open inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	open inline val actionFunction: Function<Unit>
) {

	fun getMismatchedArgTypes(args: List<Any?>): List<Pair<String, String>> {
		val mismatchedArgTypes: MutableList<Pair<String, String>> = mutableListOf()
		(args zip argumentTypes).forEach { (argument, type) ->
			if (!type.isInstance(argument)) {
				mismatchedArgTypes.add(argument.toString() to type.simpleName as String)
			}
		}
		return mismatchedArgTypes.toList()
	}

	val actionSignature: String = argumentTypes.toString()
	val actionHash: Int = argumentTypes.hashCode()

	fun allArgTypesMatch(args: List<Any?>): Boolean =
		if (args.size != argumentCount)
			false
		else
			(args zip argumentTypes).all { (argument, type) ->
				type.isInstance(argument)
			}


	operator fun invoke(subject: SUBJECT, vararg args: Any?) = executeAction(subject, args.asList())

	private fun executeAction(subject: SUBJECT, args: List<Any?>) {
		if (args.size != argumentCount) {
			return onInvalidArgumentCount(args.size)
		}
		if (!allArgTypesMatch(args)) {
			return onInvalidArgumentTypes(getMismatchedArgTypes(args))
		}

		subjectRequirements.forEach { requirement ->
			if (!requirement.meetsRequirement(subject)) {
				return requirement.doesntMeetRequirement(subject)
			}
		}

		return when (this) {
			is Action0 -> this.execute(subject)
			is Action1<SUBJECT, *> -> this.execute(subject, args[0])
			is Action2<SUBJECT, *, *> -> this.execute(subject, args[0], args[1])
			is Action3<SUBJECT, *, *, *> -> this.execute(subject, args[0], args[1], args[2])
			is Action4<SUBJECT, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3])
			is Action5<SUBJECT, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4])
			is Action6<SUBJECT, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5])
			is Action7<SUBJECT, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6])
			is Action8<SUBJECT, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7])
			is Action9<SUBJECT, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8])
			is Action10<SUBJECT, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9])
			is Action11<SUBJECT, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10])
			is Action12<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11])
			is Action13<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12])
			is Action14<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13])
			is Action15<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14])
			is Action16<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15])
			is Action17<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16])
			is Action18<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17])
			is Action19<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18])
			is Action20<SUBJECT, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *> -> this.execute(subject, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19])
		}
	}
}

class Action0<SUBJECT>(
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.() -> Unit
): Action<SUBJECT>(
	argumentCount = 0,
	argumentTypes = listOf(),
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT) =
		actionFunction(subject)
}

class Action1<SUBJECT, A: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A) -> Unit
): Action<SUBJECT>(
	argumentCount = 1,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?) =
		actionFunction(subject, arg0 as A)
}

class Action2<SUBJECT, A: Any, B: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B) -> Unit
): Action<SUBJECT>(
	argumentCount = 2,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B)
}

class Action3<SUBJECT, A: Any, B: Any, C: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C) -> Unit
): Action<SUBJECT>(
	argumentCount = 3,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C)
}

class Action4<SUBJECT, A: Any, B: Any, C: Any, D: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D) -> Unit
): Action<SUBJECT>(
	argumentCount = 4,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D)
}

class Action5<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E) -> Unit
): Action<SUBJECT>(
	argumentCount = 5,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E)
}

class Action6<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F) -> Unit
): Action<SUBJECT>(
	argumentCount = 6,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F)
}

class Action7<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G) -> Unit
): Action<SUBJECT>(
	argumentCount = 7,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G)
}

class Action8<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H) -> Unit
): Action<SUBJECT>(
	argumentCount = 8,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H)
}

class Action9<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I) -> Unit
): Action<SUBJECT>(
	argumentCount = 9,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I)
}

class Action10<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J) -> Unit
): Action<SUBJECT>(
	argumentCount = 10,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J)
}

class Action11<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K) -> Unit
): Action<SUBJECT>(
	argumentCount = 11,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K)
}

class Action12<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L) -> Unit
): Action<SUBJECT>(
	argumentCount = 12,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L)
}

class Action13<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M) -> Unit
): Action<SUBJECT>(
	argumentCount = 13,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M)
}

class Action14<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N) -> Unit
): Action<SUBJECT>(
	argumentCount = 14,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N)
}

class Action15<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any, O: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O) -> Unit
): Action<SUBJECT>(
	argumentCount = 15,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?, arg14: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N, arg14 as O)
}

class Action16<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any, O: Any, P: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P) -> Unit
): Action<SUBJECT>(
	argumentCount = 16,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?, arg14: Any?, arg15: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N, arg14 as O, arg15 as P)
}

class Action17<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any, O: Any, P: Any, Q: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q) -> Unit
): Action<SUBJECT>(
	argumentCount = 17,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?, arg14: Any?, arg15: Any?, arg16: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N, arg14 as O, arg15 as P, arg16 as Q)
}

class Action18<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any, O: Any, P: Any, Q: Any, R: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R) -> Unit
): Action<SUBJECT>(
	argumentCount = 18,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?, arg14: Any?, arg15: Any?, arg16: Any?, arg17: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N, arg14 as O, arg15 as P, arg16 as Q, arg17 as R)
}

class Action19<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any, O: Any, P: Any, Q: Any, R: Any, S: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S) -> Unit
): Action<SUBJECT>(
	argumentCount = 19,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?, arg14: Any?, arg15: Any?, arg16: Any?, arg17: Any?, arg18: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N, arg14 as O, arg15 as P, arg16 as Q, arg17 as R, arg18 as S)
}

class Action20<SUBJECT, A: Any, B: Any, C: Any, D: Any, E: Any, F: Any, G: Any, H: Any, I: Any, J: Any, K: Any, L: Any, M: Any, N: Any, O: Any, P: Any, Q: Any, R: Any, S: Any, T: Any>(
	argumentTypes: List<KClass<out Any>>,
	subjectRequirements: List<Requirement<SUBJECT>> = listOf(),
	override inline val onInvalidArgumentCount: (Int) -> Unit = {},
	override inline val onInvalidArgumentTypes: (List<Pair<String, String>>) -> Unit = {},
	override inline val actionFunction: SUBJECT.(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T) -> Unit
): Action<SUBJECT>(
	argumentCount = 20,
	argumentTypes = argumentTypes,
	subjectRequirements = subjectRequirements,
	onInvalidArgumentCount = onInvalidArgumentCount,
	onInvalidArgumentTypes = onInvalidArgumentTypes,
	actionFunction = actionFunction
) {
	fun execute(subject: SUBJECT, arg0: Any?, arg1: Any?, arg2: Any?, arg3: Any?, arg4: Any?, arg5: Any?, arg6: Any?, arg7: Any?, arg8: Any?, arg9: Any?, arg10: Any?, arg11: Any?, arg12: Any?, arg13: Any?, arg14: Any?, arg15: Any?, arg16: Any?, arg17: Any?, arg18: Any?, arg19: Any?) =
		actionFunction(subject, arg0 as A, arg1 as B, arg2 as C, arg3 as D, arg4 as E, arg5 as F, arg6 as G, arg7 as H, arg8 as I, arg9 as J, arg10 as K, arg11 as L, arg12 as M, arg13 as N, arg14 as O, arg15 as P, arg16 as Q, arg17 as R, arg18 as S, arg19 as T)
}

