package action.requirement

inline fun <T> Requirement(
    builder: RequirementBuilder<T>.() -> Unit
) = RequirementBuilder<T>().apply(builder).build()
