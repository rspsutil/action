package action.requirement

class Requirement<T>(
    inline val doesntMeetRequirement: T.() -> Unit,
    inline val meetsRequirement: T.() -> Boolean
)
