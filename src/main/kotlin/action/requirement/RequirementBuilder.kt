package action.requirement

class RequirementBuilder<SUBJECT> {
    private val DEFAULT_MEET: SUBJECT.() -> Boolean = {true}
    var meetsRequirement: SUBJECT.() -> Boolean = DEFAULT_MEET
    fun meetsRequirement(func: SUBJECT.() -> Boolean) {
        if (meetsRequirement == DEFAULT_MEET) {
            meetsRequirement = func
        }
    }


    private val DEFAULT_NOT: SUBJECT.() -> Unit = {}
    var doesntMeetRequirement: SUBJECT.() -> Unit = DEFAULT_NOT
    fun doesntMeetRequirement(func: SUBJECT.() -> Unit) {
        if (doesntMeetRequirement == DEFAULT_NOT) {
            doesntMeetRequirement = func
        }
    }

    fun build(): Requirement<SUBJECT> = Requirement(doesntMeetRequirement, meetsRequirement)
}